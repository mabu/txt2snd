Simple tools to create awesome ringtone from characters.

# txt2mrs

`txt2mrs` converts a text into some morse codes

For instance `hello world` will be translated to `.... . .-.. .-.. --- / .-- --- .-. .-.. -..`

All input characters not in [a-zA-Z0-9] are dismissed.

usage sample:

    echo "hello" | txt2mrs

# mrs2snd

`mrs2snd` converts morse code into wav file.

usage sample:

    echo "...---..." | mrs2snd -o sos.wav


# generate a ringtone

To generate a cool ringtone, pipe the two programs

    echo "mum" | txt2mrs | mrs2snd -o mum.wav

# build

You can try to build with

    ./configure && make

If the system complains about autotools version, type

    autoreconf --install

If the system complains about sndfile.h missing, install it:

    apt install libsndfile1-dev

