/* txt2mrs */

/**
 * This program is a free software,
 * Do what you want with it, I don't care.
 * This program comes "as is", without any warranty
 * */

#include <ctype.h>
#include <stdio.h>

const char * mrs_numbers[] = 
{
    "----- ", ".---- ", "..--- ", "...-- ", "....- ", 
    "..... ", "-.... ", "--... ", "---.. ", "----. "
};

const char *mrs_letters[] = 
{
    ".- ",    "-... ",  "-.-. ",  "-.. ",   ". ", 
    "..-. ",  "--. ",   ".... ",  ".. ",    ".--- ",  
    "-.- ",   ".-.. ",  "-- ",    "-. ",    "--- ", 
    ".--. ",  "--.- ",  ".-. ",   "... ",   "- ", 
    "..- ",   "...- ",  ".-- ",   "-..- ",  "-.-- ", 
    "--.. "
};


const char * txt2mrs(char c)
{    
    if (isdigit(c)) 
    {
        return mrs_numbers[c-'0'];
    }
    if (isspace(c)) 
    {
        return "/ ";
    }
    if (isalpha(c)) 
    {
        return mrs_letters[tolower(c)-'a'];
    }

    return "";
}

int main(void)
{
    int c = getchar();

    while (EOF != c)
    {
        printf("%s", txt2mrs(c));
        fflush(stdout);
        c = getchar();
    }
    puts("");
    return 0;
}
