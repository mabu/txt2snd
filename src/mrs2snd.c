/**
 * This program is a free software,
 * Do what you want with it, I don't care.
 * This program comes "as is", without any warranty
 * */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <sndfile.h>

#ifndef _WIN32
#include <unistd.h>
#else
#define STDOUT_FILENO 1
#endif

typedef struct
{
    /* ms */
    int base_time;
    /* Hz */
    int sample_rate;
    /* Hz */
    int sound_freq;

    char *sz_out;

} param_t;

const param_t DEFAULTS_PARAM = { 50, 22050, 1500, NULL};

typedef struct
{
    void *snd_on;
    void *snd_off;

    size_t frame_cnt;

    SNDFILE *sf_out;
} data_t;


void usage(const char *name)
{
    printf("usage: %s [options]\n", name);
    printf("  options are\n");
    printf("  -h: display this help\n");
    printf("  -c <base time (ms)> <sample rate (Hz)> <frequency (Hz)>: configuration\n");
    printf("  -o <filename>: file to write\n");
}

#define ERROR_MISSING_ARG(c)    \
    fprintf(stderr, "\nerror: no file name after -%c option\n\n", c);   \
    err = 2;    \
    break;

int parse_cmd_line(int argc, char *argv[], param_t *param)
{
    int err = 0;
    int i;

    for (i = 0; i < argc; ++i)
    {
        if ('-' == argv[i][0])
        {
            switch (argv[i][1])
            {
            case 'c':
                /* read next 3 arg as filenames */
                i+=3;
                if (i >= argc)
                {
                    ERROR_MISSING_ARG( argv[i-3][1] )
                }
                param->base_time = strtol(argv[i-2], NULL, 10);
                param->sample_rate = strtol(argv[i-1], NULL, 10);
                param->sound_freq = strtol(argv[i], NULL, 10);
                break;
            case 'o':
                /* read next arg as filename */
                ++i;
                if (i >= argc)
                {
                    ERROR_MISSING_ARG( argv[i-1][1] )
                }
                param->sz_out = argv[i];
                break;
            case 'h':
                err = 1;
                break;
            }
        }
        if (err)
        {
            break;
        }
    }

    return err;
}

int prepare(param_t param, data_t *data)
{
    SF_INFO info;
    size_t snd_size;

    /* create output file */
    info.samplerate = param.sample_rate;
    info.channels = 1;
    info.sections = 1;
    info.seekable = 0;
    info.frames = 0;


    if (NULL == param.sz_out)
    {
        info.frames = 0x7FFFFFFF;
        info.format = SF_FORMAT_AU | SF_FORMAT_PCM_32;
        data->sf_out = sf_open_fd(STDOUT_FILENO, SFM_WRITE, &info, 0);
    }
    else
    {
        info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
        data->sf_out = sf_open(param.sz_out, SFM_WRITE, &info);
    }

    if (NULL == data->sf_out)
    {
        fprintf(stderr, "error: cannot open \"%s\" for writing\n", param.sz_out);
        return 1;
    }


    /* create snd on */
    data->frame_cnt = param.sample_rate * param.base_time / 1000;
    snd_size = sizeof(short) * info.channels * data->frame_cnt;

    data->snd_on = malloc(snd_size);
    if (NULL == data->snd_on)
    {
        perror("malloc");
        return 1;
    }
    /* fill snd_on */
    {
        int cnt = info.channels * data->frame_cnt;
        int i;
        short *dat = data->snd_on;

        for (i = 0; i < cnt; ++i)
        {
            double x = (double) i / (double) param.sample_rate;

            dat[i] = 20000. * sin(x *(double) param.sound_freq * 2.*3.14159);
        }

    }

    /* create snd off */
    data->snd_off = malloc(snd_size);
    if (NULL == data->snd_off)
    {
        perror("malloc");
        return 1;
    }
    memset(data->snd_off, 0, snd_size);

    return 0;
}

int process(data_t data)
{
    int i;
    int c = getchar();

    while (EOF != c)
    {
        // analyse c : '.' '-' ou '/'
        switch (c)
        {
        /* ti : 1 + 1*/
          case '.':
            sf_write_short(data.sf_out, data.snd_on, data.frame_cnt);
            sf_write_short(data.sf_out, data.snd_off, data.frame_cnt);
            break;
        /* ta : 3 + 1 */
          case '-':
            for (i = 0; i < 3; ++i)
            {
                sf_write_short(data.sf_out, data.snd_on, data.frame_cnt);
            }
            sf_write_short(data.sf_out, data.snd_off, data.frame_cnt);
            break;
        /* inter lettre : 2 */
          case ' ':
            for (i = 0; i < 2; ++i)
            {
                sf_write_short(data.sf_out, data.snd_off, data.frame_cnt);
            }
            break;
        /* inter mot : 6 */
          case '/':
            /* sf_write_short(data.sf_out, data.snd_off, data.frame_cnt); */
            for (i = 0; i < 6; ++i)
            {
                sf_write_short(data.sf_out, data.snd_off, data.frame_cnt);
            }
            break;
        }

        c = getchar();
    }

    return 0;
}

int cleanup(data_t *data)
{
    free(data->snd_on);
    free(data->snd_off);

    if (NULL != data->sf_out)
    {
        sf_close(data->sf_out);
        data->sf_out = NULL;
    }

    return 0;
}

int main(int argc, char *argv[])
{
    do
    {
        param_t param = DEFAULTS_PARAM;
        data_t data;

        if (0 != parse_cmd_line(argc, argv, &param))
        {
            usage(*argv);
            break;
        }

        if (0 != prepare(param, &data))
        {
            break;
        }

        if (0 != process(data))
        {
            break;
        }

        if (0 != cleanup(&data))
        {
            break;
        }

    } while (0);

    return 0;
}
